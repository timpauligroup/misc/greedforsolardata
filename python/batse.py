import itertools as it
from datetime import datetime
from glob import iglob

import pandas as pd
from midiutil import MIDIFile


# read data
def read_months():
    months = []
    foldername = 'data/batse/*'
    # get
    for fn in sorted(iglob(foldername)):
        for f in sorted(iglob(fn + '/*.list')):
            try:
                tmp = pd.read_csv(f, sep='\s{2,}',
                                  skiprows=[0, 1, 2, 3, 4, 6, 7, 8], parse_dates=True, header=[0], engine='python')
                months.append(tmp)
            except pd.errors.EmptyDataError:
                print("empty", f)
    # beautify
    for m in months:
        m.columns = ['Peak rate' if c == 'Peak.1' else c for c in m.columns]
        m.rename(columns={"BATSE": "number",
                          'Start': "start date",
                          'Start.1': "start time",
                          'Peak': "peak time",
                          'Duration': "duration",
                          'Peak rate': "peaks",
                          'Burst': "bursts", },
                 inplace=True)
        m.drop(columns=['Triggertime'], inplace=True)
        try:
            m.drop(columns=['Total'], inplace=True)
        except KeyError:
            print("no total")
    months = [m.fillna(0) for m in months]
    months = [m.replace('Unknown', 0) for m in months]
    for i in range(len(months)):
        months[i]['peaks'] = months[i]['peaks'].apply(pd.to_numeric)
        for j in range(len(months[i]['peak time'])):
            starttime = datetime.strptime(
                months[i]['start time'][j], '%H%M:%S')
            peaktime = datetime.strptime(months[i]['peak time'][j], '%H%M:%S')
            difftime = (peaktime - starttime).seconds
            months[i].at[j, 'peak time'] = difftime
    print(months[0].columns)
    return months


def basschords(months, glob):
    step = 65535
    mfile = MIDIFile(file_format=2, deinterleave=False,
                     ticks_per_quarternote=step, eventtime_is_ticks=True)
    time = 0
    if glob:  # dirty
        maxpeaks = max([max(m['peaks']) for m in months])
        maxduration = max([max(m['duration']) for m in months])
        maxbursts = max([max(m['bursts']) for m in months])
        maxptime = max([max(m['peak time']) for m in months])
    for m in months:
        if not glob:  # dirty 2
            maxpeaks = max(m['peaks'])
            maxduration = max(m['duration'])
            maxbursts = max(m['bursts'])
            maxptime = max(m['peak time'])
        notecounter = 0
        for p, dur, b, n, pt in zip(m['peaks'], m['duration'], m['bursts'], m['number'], m['peak time']):
            maxpitch = scale(0, 127, 32, 96, (n % 128))
            difftime = int(scale(0, maxptime, 0, step / 128.0, pt))
            pitch = int(scale(0, maxpeaks, 0, maxpitch, p))
            dur = int(scale(0, maxduration, 1, step, dur))
            dur = (step - difftime - 1) if (time + difftime + dur) > (time + step) else dur
            vel = int(scale(0, maxbursts, 64, 127, b))
            mfile.addNote(0, 0, pitch, time + difftime, dur, vel)
            notecounter += 1
        if notecounter > 2:
            time += step
    with open('build/midi/basschords' + str(glob) + '.mid', "wb") as file:
        mfile.writeFile(file)


def footsteps(months, train, minvel):
    step = 65535
    mfile = MIDIFile(file_format=2, deinterleave=False,
                     ticks_per_quarternote=step, eventtime_is_ticks=True)
    time = 0
    oldpitch = None
    if train:
        pitchpattern = it.cycle([(32 + (4 * i), 32 + (4 * (i + 1)))
                                 for i in range(4)])
    else:
        pitchpattern = it.cycle([(32, 40)])
    for m in months:
        maxpeaks = max(m['peaks'])
        maxduration = max(m['duration'])
        maxbursts = max(m['bursts'])
        maxptime = max(m['peak time'])
        for p, dur, b, pt, pp in zip(m['peaks'], m['duration'], m['bursts'], m['peak time'], pitchpattern):
            difftime = int(scale(0, maxptime, 0, step / 256.0, pt))
            pitch = int(scale(0, maxpeaks, pp[0], pp[1], p))
            dur = int(scale(0, maxduration, step / 2.0, step, dur))
            dur = (step - difftime - 1) if (time + difftime + dur) > (time + step) else dur
            vel = int(scale(0, maxbursts, minvel, 127, b))
            if pitch != oldpitch:
                mfile.addNote(0, 0, pitch, time + difftime, dur, vel)
                oldpitch = pitch
                time += step
    with open('build/midi/footsteps' + str(train) + str(minvel) + '.mid', "wb") as file:
        mfile.writeFile(file)


def ligeti(months):
    ligeti_field(months, range(8), 1,
                 [i for i in range(1, 9)], 'ligeti01_')
    ligeti_field(months, range(8, 16), 65535,
                 [-i for i in range(9, 1, -1)], 'ligeti02_')


def windarps(months):
    ligeti_field(months, range(8), 1,
                 [i for i in range(1, 9)], 'windarps', norepitions=True)


def ligeti_field(months, channels, startdur, steps, name, norepitions=False):
    for channel, step, startpitch in zip(channels, steps, range(12, 127, 12)):
        mfile = MIDIFile(file_format=2, deinterleave=False,
                         ticks_per_quarternote=65535, eventtime_is_ticks=True)
        ligeti_voice(startdur, step, startpitch, months,
                     channel, mfile, norepitions)
        with open('build/midi/' + name + str(channel) + ".mid", "wb") as file:
            mfile.writeFile(file)


def ligeti_voice(startdur, step, startpitch, months, channel, mfile, norepitions=False):
    print('channel: ', channel)
    time = 0
    thisdur = startdur
    for m in it.cycle(months):
        maxpeaks = max(m['peaks'])
        maxduration = max(m['duration'])
        maxbursts = max(m['bursts'])
        oldpitch = None
        for p, dur, b in zip(m['peaks'], m['duration'], m['bursts']):
            maxpitch = int(
                scale(0, maxbursts, startpitch, startpitch + 12, b))
            pitch = int(scale(0, maxpeaks, startpitch, maxpitch, p))
            vel = int(scale(0, maxduration, 64, 127, dur))
            if oldpitch != pitch:
                if thisdur >= 1:
                    mfile.addNote(0, 0, pitch, time, thisdur, vel)
            time += thisdur
            thisdur += step
            if norepitions:
                oldpitch = pitch
        if time > gausum(65535) or thisdur < 1:
            break


# utility
def scale(oldstart, oldend, newstart, newend, value):
    if value > oldend:
        value = oldend
    if value < oldstart:
        value = oldstart
    oldrange = oldend - oldstart
    newrange = newend - newstart
    if oldrange != 0:
        scaler = newrange / oldrange
    else:
        scaler = 0
    return newstart + ((value - oldstart) * scaler)


def gausum(n):
    return int(((n * n) + n) / 2)
